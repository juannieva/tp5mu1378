package situacion2;


import situacion2.IProducto;
import situacion2.Empresa;
import java.math.BigDecimal;
import java.util.ArrayList;

public class OrdenDeVenta {
    private Integer codigo;
    private ArrayList<IProducto> productosEnOrdendeventa=new ArrayList<IProducto>();

    public OrdenDeVenta(ArrayList<IProducto> productos) {
        this.productosEnOrdendeventa=productos;
    }
    
    public void setCodigo(Integer codigo){
        this.codigo=codigo;
    }

    public Integer getCodigo(){
        return codigo;
    }
    
    public ArrayList<IProducto> getProductos() {
        return productosEnOrdendeventa;
    }

    public Integer getNumProductos() {
        return productosEnOrdendeventa.size();
    }


    public BigDecimal getPrecioTotal(){
        BigDecimal aux2=new BigDecimal("0");
        for(IProducto aux: productosEnOrdendeventa){
            aux2= aux2.add(aux.getPrecio().multiply(new BigDecimal(aux.getCantidad())));
        }
        return aux2;
    }


    public void agregarProductos(IProducto producto){
        for(IProducto aux: productosEnOrdendeventa){
            if(aux.equals(producto)){
                
            }
        }
        productosEnOrdendeventa.add(producto);
        
    }

}
