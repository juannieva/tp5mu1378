package situacion2;

import java.math.BigDecimal;

public class Proveedor {
    private BigDecimal cuit;
    private String nombre;
    private String razonSocal;

    public Proveedor(BigDecimal cuit, String nombre, String razonSocal) {
        this.cuit = cuit;
        this.nombre = nombre;
        this.razonSocal = razonSocal;
    }


    public BigDecimal getCuit() {
        return cuit;
    }

    public void setCuit(BigDecimal cuit) {
        this.cuit = cuit;
    }

    public String getRazonSocal() {
        return razonSocal;
    }

    public void setRazonSocal(String razonSocal) {
        this.razonSocal = razonSocal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void mostrarDatos(){
        System.out.println("//Datos Proveedor//"+"\nNombre: "+getNombre()+"\nCuit: "+getCuit());
    }
    
}
