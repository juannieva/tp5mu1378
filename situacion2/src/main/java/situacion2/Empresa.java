package situacion2;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import situacion2.persistencia.Arrays;

public class Empresa {
    private String nombre;
    private Arrays persistencia;

    public Empresa(String nombre, Arrays persistencia) {
        this.nombre = nombre;
        this.persistencia = persistencia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<OrdenDeVenta> getOrdenesVenta() {
        return (ArrayList<OrdenDeVenta>) persistencia.get(Integer.parseInt("0")).findAll();
    }

    public void agregarOrdenesDeVenta(OrdenDeVenta ordenDeVenta) {
        ordenDeVenta.setCodigo((persistencia.get(Integer.parseInt("0")).findAll().size())+1);
        persistencia.get(Integer.parseInt("0")).insert(ordenDeVenta);
    }
    
    public void agregarProveedor(Proveedor proveedor) {
        persistencia.get(Integer.parseInt("2")).insert(proveedor);
    }
    
    public ArrayList<Proveedor> getProveedores(){
         return (ArrayList<Proveedor>)persistencia.get(Integer.parseInt("2")).findAll();
    }
    
    public void agregarProducto(IProducto producto){
        persistencia.get(Integer.parseInt("1")).insert(producto);
    }
    
    public void eliminarProducto(IProducto producto){
        persistencia.get(Integer.parseInt("1")).delete(producto.getCodigo().toString());
    }
    
    public ArrayList<IProducto> getProductos(){
        return (ArrayList<IProducto>)persistencia.get(Integer.parseInt("1")).findAll();
    }

   public Proveedor getProveedor(Proveedor aux){
       return (Proveedor)persistencia.get(2).findByObject(aux);
   }
   
   public Proveedor getProveedor(BigDecimal aux){
       return (Proveedor)persistencia.get(2).findByPK(aux);
   }

   public IProducto getProducto(Integer aux){
       return (IProducto)persistencia.get(Integer.parseInt("1")).findByPK(aux);
   }

   public Integer getNumProductos(){
        return persistencia.get(1).findAll().size();
    }

    public Integer getNumProveedores(){
        return persistencia.get(2).findAll().size();
    }
    
    public Integer getNumOrdendeVenta(){
        return persistencia.get(0).findAll().size();
    }

   
   public void modificarStockProducto(IProducto producto,Integer numStock){
       IProducto original,modificado;
       original=producto;
       modificado=producto;
       persistencia.get(Integer.parseInt("1")).delete(original.getCodigo().toString());
       modificado.setCantidad(numStock);
       persistencia.get(Integer.parseInt("1")).insert(modificado);
   }
   
   public OrdenDeVenta getOrdenVenta(Integer codigo) {
        return (OrdenDeVenta) persistencia.get(Integer.parseInt("0")).findByPK(codigo);
   }
   
   public ArrayList<Producto> ordenarProductosxPrecio(){
        ArrayList<Producto> productos = (ArrayList<Producto>) persistencia.get(1).findAll();
        Collections.sort(productos);
        return productos;
    }
   
   public ArrayList<Producto> ordenarProductosxNombre(){
        ArrayList<Producto> productos = (ArrayList<Producto>) persistencia.get(1).findAll();
        Collections.sort(productos, new CompararProductosxNombre());
        return productos;
    }
}