/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package situacion2.persistencia;

import java.util.ArrayList;

/**
 *
 * @author Juan
 */
public class Arrays {
        ArrayList<IArrays> arrays;
    public Arrays(){
        arrays= new ArrayList<IArrays>();
        IArrays arrayOrdenesdeVenta= new ArrayOrdenesdeVenta();
        IArrays arrayProductos= new ArrayProductos();
        IArrays arrayProveedores= new ArrayProveedores();
        arrays.add(arrayOrdenesdeVenta);
        arrays.add(arrayProductos);
        arrays.add(arrayProveedores);
    }
    
    public IArrays get(Integer i){
        return arrays.get(i);
    }
}
