
package situacion2.persistencia;

import java.util.Collection;


/**
 *
 * @author Juan
 */
public interface IArrays {
    public void delete(String vnumero);
    public Object findByPK(Object vnumero);
    public Object findByObject(Object aux);
    public Collection findAll();
    public void insert(Object insertRecord);
    public void update(Object originalRecord,Object updateRecord);
}
