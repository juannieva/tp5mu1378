/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package situacion2.persistencia;

import java.util.ArrayList;
import java.util.Collection;
import situacion2.IProducto;
import situacion2.Producto;
import situacion2.excepciones.CodigoProductoYaExiste;

/**
 *
 * @author Juan
 */
public class ArrayProductos implements IArrays {
    private ArrayList<IProducto> productos = new ArrayList<IProducto>();
    
    @Override
    public void delete(String codigo) {
       IProducto existe=null;
       existe = findByPK(Integer.parseInt(codigo));
       if (existe != null) {
            productos.remove(existe);
        }        
    }
    
    @Override
    public IProducto findByPK(Object codigo)  {
        Integer aux1=(Integer)codigo;
        IProducto resultado = null;
         for (IProducto aux : productos) {
            if (aux.getCodigo().equals(aux1)){
                resultado=aux;
            }
        }
        if(resultado==null){
            //throw new ClienteNoEncontradoExceptio("Cliente DNI:"+dni+" no encontrado");
        }
        return resultado;
    }
    
    

    @Override
    public Collection findAll() {
        return productos;
    }
    

    @Override
    public void insert(Object insertRecord) {
        IProducto existe=null;
        IProducto aux1=(Producto) insertRecord;
        for (IProducto aux : productos) {
            if (aux.getCodigo().equals(aux1.getCodigo())){
                existe=aux;
            }
        }
        if (existe != null) {
            throw new CodigoProductoYaExiste("Ya existe un producto con codigo :" + aux1.getCodigo());
        }
        
        productos.add(aux1);
    }

    @Override
    public void update(Object originalRecord,Object updateRecord) {
         IProducto aux=(IProducto)originalRecord;
         String codigo=String.valueOf(aux.getCodigo());
         delete(codigo);
         insert(updateRecord);
    }

    @Override
    public Object findByObject(Object aux) {
        IProducto aux1=(Producto)aux;
        IProducto resultado = null;
        for (IProducto var: productos) {
            if (var.equals(aux1)){
                resultado=aux1;
            }
        }
        if(resultado==null){
            //throw new ClienteNoEncontradoExceptio("Cliente DNI:"+dni+" no encontrado");
        }
        return resultado;
    }

    
}
