/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package situacion2.persistencia;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import situacion2.Proveedor;

/**
 *
 * @author Juan
 */
public class ArrayProveedores implements IArrays {
    private ArrayList<Proveedor> proveedores = new ArrayList<Proveedor>();
    
    @Override
    public void delete(String dni) {
       Proveedor existe=null;
       existe = findByPK(Integer.parseInt(dni));
       if (existe != null) {
            proveedores.remove(existe);            
            
        }        
    }
    @Override
    public Proveedor findByPK(Object cui)  {
        BigDecimal cuit= new BigDecimal(cui.toString());
        Proveedor resultado = null;
         for (Proveedor aux : proveedores) {
            if (aux.getCuit().equals(cuit)){
                resultado=aux;
            }
        }
        if(resultado==null){
            //throw new ClienteNoEncontradoExceptio("Cliente DNI:"+dni+" no encontrado");
        }
        return resultado;
    }
    
    

    @Override
    public Collection findAll() {
        return proveedores;
    }
    

    @Override
    public void insert(Object insertRecord) {
        Proveedor existe=null;
        Proveedor aux1=(Proveedor) insertRecord;
        for (Proveedor aux : proveedores) {
            if (aux.getCuit().equals(aux1.getCuit())){
                existe=aux;
            }
        }
        if (existe != null) {
            //throw new ClienteExistenteException("Cliente existente DNI:" + existe.getDni().toString());
        }
        proveedores.add(aux1);
    }

    @Override
    public void update(Object originalRecord,Object updateRecord) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

  

    @Override
    public Object findByObject(Object aux1) {
        Proveedor aux=(Proveedor)aux1;
        Proveedor resultado = null;
         for (Proveedor var : proveedores) {
            if (var.equals(aux)){
                resultado=aux;
            }
        }
        if(resultado==null){
            //throw new ClienteNoEncontradoExceptio("Cliente DNI:"+dni+" no encontrado");
        }
        return resultado;
    }

    
}
