 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package situacion2.persistencia;

import java.util.ArrayList;
import java.util.Collection;
import situacion2.OrdenDeVenta;


/**
 *
 * @author Juan
 */
public class ArrayOrdenesdeVenta implements IArrays {
    private ArrayList<OrdenDeVenta> ordendesdeVenta = new ArrayList<OrdenDeVenta>();
    
    @Override
    public void delete(String dni) {
       OrdenDeVenta existe=null;
       existe = findByPK(Integer.parseInt(dni));
       if (existe != null) {
            ordendesdeVenta.remove(existe);            
        }        
    }
    
    @Override
    public OrdenDeVenta findByPK(Object codigo)  {
        Integer aux1=(Integer)codigo;
        OrdenDeVenta resultado = null;
         for (OrdenDeVenta aux : ordendesdeVenta) {
            if (aux.getCodigo().equals(aux1)){
                resultado=aux;
            }
        }
        if(resultado==null){
            //throw new ClienteNoEncontradoExceptio("Cliente DNI:"+dni+" no encontrado");
        }
        return resultado;
    }
    
    

    @Override
    public Collection findAll() {
        return ordendesdeVenta;
    }
    

    @Override
    public void insert(Object insertRecord) {
        OrdenDeVenta existe=null;
        OrdenDeVenta aux1=(OrdenDeVenta) insertRecord;
        for (OrdenDeVenta aux : ordendesdeVenta) {
            if (aux.getCodigo().equals(aux1.getCodigo())){
                existe=aux;
            }
        }
        if (existe != null) {
            //throw new ClienteExistenteException("Cliente existente DNI:" + existe.getDni().toString());
        }
        ordendesdeVenta.add(aux1);
    }

    @Override
    public void update(Object originalRecord,Object updateRecord) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object findByObject(Object aux) {
        OrdenDeVenta aux1=(OrdenDeVenta)aux;
        OrdenDeVenta resultado = null;
         for (OrdenDeVenta var : ordendesdeVenta) {
            if (var.getCodigo().equals(aux1)){
                resultado=aux1;
            }
        }
        if(resultado==null){
            //throw new ClienteNoEncontradoExceptio("Cliente DNI:"+dni+" no encontrado");
        }
        return resultado;
    }

    
}
