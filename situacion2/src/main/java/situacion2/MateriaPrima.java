package situacion2;

import java.math.BigDecimal;

public class MateriaPrima extends Producto {
    private Proveedor proveedor;

    
    public MateriaPrima(Integer codigo, String nombre,Integer cantidad ,BigDecimal precio, Proveedor proveedor) {
        super(codigo, nombre,cantidad, precio);
        this.proveedor=proveedor;
    }
    

    @Override
    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    @Override
    public void mostrarProducto() {
        System.out.println("Codigo: " + getCodigo() + "\nNombre: " + getNombre() + "\nPrecio: " + getPrecio()
                + "\n-------------------------------------------------------");

    }

    @Override
    public BigDecimal getConsumo() {
        return new BigDecimal("0");
    }

    @Override
    public String getFuncionalidad() {
        return "--";
    }

    @Override
    public String getTipo() {
        return "Materia Prima";
    }
    
}
