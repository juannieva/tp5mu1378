/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package situacion2;

import java.math.BigDecimal;
import java.util.ArrayList;
import situacion2.persistencia.Arrays;
import situacion2.ui.UIVentanaPrincipal;

public class App {
 
     public static final void main(String argv[]) {
        Arrays persistencia= new Arrays();
        Proveedor proveedor= new Proveedor(new BigDecimal("2065473"),"Jose Luis", "Comerciante");
        Proveedor proveedor1= new Proveedor(new BigDecimal("10654732"),"Marcelo Diaz", "Comerciante");
        persistencia.get(2).insert(proveedor);
        persistencia.get(2).insert(proveedor1);
        IProducto materiaPrima1= new MateriaPrima(Integer.parseInt("56"), "Arena", Integer.parseInt("2"), new BigDecimal(250), (Proveedor) persistencia.get(2).findByPK(Integer.parseInt("2065473")));
        IProducto materialFabricado1= new MaterialFabricado(Integer.parseInt("854"), "Cemento",Integer.parseInt("500") ,new BigDecimal(250));
        IProducto herramientaManual1= new HerramientaManual(Integer.parseInt("22"), "Tenaza",Integer.parseInt("340"), new BigDecimal (70), "Sacar Clavos");
        IProducto herramientaElectrica1= new HerramientaElectrica(Integer.parseInt("521"), "Atornillador",Integer.parseInt("149"), new BigDecimal(1500), "Ajustar Tornillos", new BigDecimal(33));
        ArrayList<IProducto> productos=new ArrayList<IProducto>();
        IProducto herramientaManual2= new HerramientaManual(Integer.parseInt("11"), "Tenaza",Integer.parseInt("3"), new BigDecimal (70), "Sacar Clavos");
        IProducto herramientaElectrica2= new HerramientaElectrica(Integer.parseInt("33"), "Atornillador",Integer.parseInt("4"), new BigDecimal(1500), "Ajustar Tornillos", new BigDecimal(33));
        productos.add(herramientaElectrica2);
        productos.add(herramientaManual2);
        OrdenDeVenta orden1=new OrdenDeVenta(productos);
        orden1.setCodigo(1);
        persistencia.get(0).insert(orden1);
        
        persistencia.get(1).insert(materiaPrima1);
        persistencia.get(1).insert(herramientaManual1);
        persistencia.get(1).insert(herramientaElectrica1);
        persistencia.get(1).insert(materialFabricado1);
        UIVentanaPrincipal v1= new UIVentanaPrincipal(persistencia);
        v1.setVisible(true);
    }
}
