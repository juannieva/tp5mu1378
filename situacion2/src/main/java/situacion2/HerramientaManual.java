package situacion2;


import java.math.BigDecimal;

public class HerramientaManual extends Herramienta {

    public HerramientaManual(Integer codigo, String nombre,Integer cantidad, BigDecimal precio, String funcionalidad) {
        super(codigo, nombre,cantidad, precio, funcionalidad);
    }

    @Override
    public void mostrarProducto() {
        System.out.println("Codigo: " + super.getCodigo() + "\nNombre: " + getNombre() + "\nPrecio: " + getPrecio()
                + "\nFuncionalidad: " + getFuncionalidad()+ "\n-------------------------------------------------------");

    }

    @Override
    public BigDecimal getConsumo() {
        return new BigDecimal("0");
    }

    @Override
    public String getTipo() {
        return "Herramienta Manual";
    }

    @Override
    public Proveedor getProveedor() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
    
}
