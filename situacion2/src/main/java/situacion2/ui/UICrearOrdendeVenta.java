/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package situacion2.ui;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Objects;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import situacion2.Empresa;
import situacion2.HerramientaElectrica;
import situacion2.HerramientaManual;
import situacion2.IProducto;
import situacion2.MaterialFabricado;
import situacion2.OrdenDeVenta;
import situacion2.excepciones.SinProductosSeleccionadosException;
import situacion2.excepciones.StockInsuficienteException;

/**
 *
 * @author Juan
 */
public class UICrearOrdendeVenta extends javax.swing.JDialog {

    DefaultTableModel modelo = new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo","Producto", "Precio", "Stock", "Cantidad"
            }
        ) {
            Class[] types = new Class [] {
                 java.lang.String.class,java.lang.String.class, java.lang.String.class, java.lang.String.class,java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false,false, false, false,true
            };
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
           

           
        };
    
    DefaultTableModel modelo1 = new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo","Producto", "Precio", "Cantidad","Precio Total"
            }
        ) {
            Class[] types = new Class [] {
                 java.lang.String.class,java.lang.String.class, java.lang.String.class, java.lang.String.class,java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false,false, false, false,true
            };
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
            
            

           
        };
    
    
    private ArrayList<IProducto> productosSeleccionados=new ArrayList<IProducto>();
    private Empresa empresa;
    
    
    public UICrearOrdendeVenta(java.awt.Frame parent, boolean modal, Empresa empresa) {
        super(parent, modal);
        this.empresa=empresa;
        initComponents();
        setTitle("Crear Orden de Venta");
        llenarTabla();
    }
    
    
    public final void llenarTabla(){
        ArrayList<IProducto> aux= new ArrayList<IProducto>();
        aux=(ArrayList<IProducto>)empresa.getProductos();
        Integer aux1=0;
        for(IProducto x: aux){
                aux1++;
                if(x.getCantidad()!=0){
                    if(x.getTipo()=="Herramienta Electrica" || x.getTipo()=="Herramienta Manual" || x.getTipo()=="Material Fabricado"){
                            Object[] fila={x.getCodigo().toString(),x.getNombre(),x.getPrecio().toString(),x.getCantidad().toString(),"0"};
                            modelo.addRow(fila);
                         } 
                }else if(x.getCantidad()==0){
                        if(x.getTipo()=="Herramienta Electrica" || x.getTipo()=="Herramienta Manual" || x.getTipo()=="Material Fabricado"){
                        Object[] fila={x.getCodigo().toString(),x.getNombre(),x.getPrecio().toString(),"SIN STOCK","0"};
                        modelo.addRow(fila);
              
                }
                    
                }
                
                    
        }
    }
    
    
    public void llenarTablaElemetosSeleccionados(){
        
        for(IProducto x: productosSeleccionados){
                BigDecimal auxPrecioTotal=x.getPrecio().multiply(new BigDecimal(x.getCantidad()));
                if("Herramienta Electrica".equals(x.getTipo()) || "Herramienta Manual".equals(x.getTipo()) || "Material Fabricado".equals(x.getTipo())){
                    Object[] fila={x.getCodigo().toString(),x.getNombre(),x.getPrecio().toString(),x.getCantidad().toString(),auxPrecioTotal.toString()};
                    modelo1.addRow(fila);
                }     
        }
    }
    
    public void segundopanel(){
        llenarTablaElemetosSeleccionados();
        BigDecimal acumuladorPrecio=new BigDecimal("0");
        for(IProducto x: productosSeleccionados){
            acumuladorPrecio=acumuladorPrecio.add(x.getPrecio().multiply(new BigDecimal(x.getCantidad())));
        }
        precioTotaljLabel.setText(acumuladorPrecio.toString());
    }
    public void actualizarStock(){
        for(int i=0; i<selecciondeProdcutosjTable.getRowCount(); i++){
                Integer aux1,aux2,aux3;
                aux1=0;
                aux2=0;
                if("SIN STOCK".equals((String) selecciondeProdcutosjTable.getValueAt(i, 3))){
                    selecciondeProdcutosjTable.setValueAt(0,i, 3);
                }else if(!"SIN STOCK".equals((String) selecciondeProdcutosjTable.getValueAt(i, 3))){
                    aux1=Integer.parseInt((String) selecciondeProdcutosjTable.getValueAt(i, 3));
                }
                if((String) selecciondeProdcutosjTable.getValueAt(i, 4)==null ||"".equals((String) selecciondeProdcutosjTable.getValueAt(i, 4))){
                    aux2=0;
                }else if((String) selecciondeProdcutosjTable.getValueAt(i, 4)!=null || !"".equals((String) selecciondeProdcutosjTable.getValueAt(i, 4))){
                        aux2=Integer.parseInt((String) selecciondeProdcutosjTable.getValueAt(i, 4));
                    }
                ;
                aux3=aux1-aux2;
                IProducto auxproducto2=empresa.getProducto(Integer.parseInt((String) selecciondeProdcutosjTable.getValueAt(i, 0)));
                empresa.modificarStockProducto(auxproducto2, aux3);
            }
    }
    
    

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jProgressBar1 = new javax.swing.JProgressBar();
        jPanel2 = new javax.swing.JPanel();
        seleccionProductosjPanel = new javax.swing.JPanel();
        seleccioneProductosjLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        selecciondeProdcutosjTable = new javax.swing.JTable();
        confirmarJButton = new javax.swing.JButton();
        cancelarJButton = new javax.swing.JButton();
        productosSeleccionadosjPanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        elementoenOrdenjLabel = new javax.swing.JLabel();
        totalaPagarjLabel = new javax.swing.JLabel();
        precioTotaljLabel = new javax.swing.JLabel();
        confirmar2jButton = new javax.swing.JButton();
        cancelar2jButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        jPanel2.setLayout(new java.awt.CardLayout());

        seleccioneProductosjLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        seleccioneProductosjLabel.setText("Seleccione Productos y cantidad a Agregar en Orden de Venta");

        selecciondeProdcutosjTable.setModel(modelo);
        jScrollPane1.setViewportView(selecciondeProdcutosjTable);

        confirmarJButton.setText("Confirmar");
        confirmarJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmarJButtonActionPerformed(evt);
            }
        });

        cancelarJButton.setText("Cancelar");
        cancelarJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelarJButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout seleccionProductosjPanelLayout = new javax.swing.GroupLayout(seleccionProductosjPanel);
        seleccionProductosjPanel.setLayout(seleccionProductosjPanelLayout);
        seleccionProductosjPanelLayout.setHorizontalGroup(
            seleccionProductosjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 466, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, seleccionProductosjPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(seleccionProductosjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, seleccionProductosjPanelLayout.createSequentialGroup()
                        .addComponent(seleccioneProductosjLabel)
                        .addGap(77, 77, 77))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, seleccionProductosjPanelLayout.createSequentialGroup()
                        .addComponent(cancelarJButton)
                        .addGap(74, 74, 74)
                        .addComponent(confirmarJButton)
                        .addGap(109, 109, 109))))
        );
        seleccionProductosjPanelLayout.setVerticalGroup(
            seleccionProductosjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, seleccionProductosjPanelLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(seleccioneProductosjLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(seleccionProductosjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelarJButton)
                    .addComponent(confirmarJButton))
                .addContainerGap())
        );

        jPanel2.add(seleccionProductosjPanel, "card2");

        jTable2.setModel(modelo1);
        jTable2.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(jTable2);
        if (jTable2.getColumnModel().getColumnCount() > 0) {
            jTable2.getColumnModel().getColumn(1).setResizable(false);
        }

        elementoenOrdenjLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        elementoenOrdenjLabel.setText("Confirme productos en Orden de Venta");
        elementoenOrdenjLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        totalaPagarjLabel.setText("Total a Pagar:");

        precioTotaljLabel.setText("Precio");

        confirmar2jButton.setText("Confirmar");
        confirmar2jButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmar2jButtonActionPerformed(evt);
            }
        });

        cancelar2jButton.setText("Cancelar");
        cancelar2jButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelar2jButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout productosSeleccionadosjPanelLayout = new javax.swing.GroupLayout(productosSeleccionadosjPanel);
        productosSeleccionadosjPanel.setLayout(productosSeleccionadosjPanelLayout);
        productosSeleccionadosjPanelLayout.setHorizontalGroup(
            productosSeleccionadosjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 466, Short.MAX_VALUE)
            .addComponent(elementoenOrdenjLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, productosSeleccionadosjPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(productosSeleccionadosjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, productosSeleccionadosjPanelLayout.createSequentialGroup()
                        .addComponent(totalaPagarjLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(precioTotaljLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(21, 21, 21))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, productosSeleccionadosjPanelLayout.createSequentialGroup()
                        .addComponent(cancelar2jButton)
                        .addGap(104, 104, 104)
                        .addComponent(confirmar2jButton)
                        .addGap(108, 108, 108))))
        );
        productosSeleccionadosjPanelLayout.setVerticalGroup(
            productosSeleccionadosjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, productosSeleccionadosjPanelLayout.createSequentialGroup()
                .addComponent(elementoenOrdenjLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 299, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(productosSeleccionadosjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(totalaPagarjLabel)
                    .addComponent(precioTotaljLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
                .addGroup(productosSeleccionadosjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(confirmar2jButton)
                    .addComponent(cancelar2jButton))
                .addContainerGap())
        );

        jPanel2.add(productosSeleccionadosjPanel, "card2");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed

    }//GEN-LAST:event_formKeyPressed

    private void confirmarJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmarJButtonActionPerformed
        try {
            for(int i=0; i<selecciondeProdcutosjTable.getRowCount(); i++) {
                Integer cantidadDisponible,cantidadIngresada,cantidadRemanente;
                cantidadDisponible=0;
                cantidadIngresada=0;
                cantidadRemanente=0;
                if("SIN STOCK".equals((String) selecciondeProdcutosjTable.getValueAt(i, 3))){
                    cantidadDisponible=0;
                }else if(!"SIN STOCK".equals((String) selecciondeProdcutosjTable.getValueAt(i, 3))){
                    cantidadDisponible=Integer.parseInt((String) selecciondeProdcutosjTable.getValueAt(i, 3));
                }
                if((String) selecciondeProdcutosjTable.getValueAt(i, 4)==null ||"".equals((String) selecciondeProdcutosjTable.getValueAt(i, 4))){
                    cantidadIngresada=0;
                }else if((String) selecciondeProdcutosjTable.getValueAt(i, 4)!=null || !"".equals((String) selecciondeProdcutosjTable.getValueAt(i, 4))){
                        cantidadIngresada=Integer.parseInt((String) selecciondeProdcutosjTable.getValueAt(i, 4));
                    }
                if(cantidadIngresada<0){
                    throw new StockInsuficienteException("Numero negativo no permitido");
                }
                cantidadRemanente=cantidadDisponible-cantidadIngresada;
                if(cantidadIngresada>cantidadDisponible){
                    throw new StockInsuficienteException("Stock Insuficiente de "+selecciondeProdcutosjTable.getValueAt(i, 1));
                }else if(!Objects.equals(cantidadRemanente, cantidadDisponible)){
                    IProducto auxproducto1=empresa.getProducto(Integer.parseInt((String) selecciondeProdcutosjTable.getValueAt(i, 0)));
                    IProducto auxproducto2=null;
                    if(auxproducto1.getTipo()=="Herramienta Electrica"){
                        auxproducto2=new HerramientaElectrica(auxproducto1.getCodigo(), auxproducto1.getNombre(),cantidadIngresada,auxproducto1.getPrecio(), auxproducto1.getFuncionalidad(), auxproducto1.getConsumo());
                    }else if(auxproducto1.getTipo()=="Herramienta Manual"){
                        auxproducto2=new HerramientaManual(auxproducto1.getCodigo(), auxproducto1.getNombre(),cantidadIngresada,auxproducto1.getPrecio(), auxproducto1.getFuncionalidad());
                    }else if(auxproducto1.getTipo()=="Material Fabricado"){
                        auxproducto2=new MaterialFabricado(auxproducto1.getCodigo(), auxproducto1.getNombre(), cantidadIngresada, auxproducto1.getPrecio());
                    }
                    productosSeleccionados.add(auxproducto2);
                }
            }
            if (productosSeleccionados.isEmpty()) {
                    throw new SinProductosSeleccionadosException("Seleccione algun producto");
            }
            seleccionProductosjPanel.setVisible(false);
            productosSeleccionadosjPanel.setVisible(true);
            segundopanel();
        } catch (StockInsuficienteException | SinProductosSeleccionadosException e) {
            JOptionPane.showMessageDialog(rootPane, e.getMessage(), "Alerta",0);
        }
    }//GEN-LAST:event_confirmarJButtonActionPerformed

    private void cancelar2jButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelar2jButtonActionPerformed
        dispose();
    }//GEN-LAST:event_cancelar2jButtonActionPerformed

    private void confirmar2jButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmar2jButtonActionPerformed
        try {
            OrdenDeVenta aux=new OrdenDeVenta(productosSeleccionados);
            empresa.agregarOrdenesDeVenta(aux);
            actualizarStock();
            dispose();
        } catch (Exception e) {
        }
    }//GEN-LAST:event_confirmar2jButtonActionPerformed

    private void cancelarJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelarJButtonActionPerformed
        dispose();
    }//GEN-LAST:event_cancelarJButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelar2jButton;
    private javax.swing.JButton cancelarJButton;
    private javax.swing.JButton confirmar2jButton;
    private javax.swing.JButton confirmarJButton;
    private javax.swing.JLabel elementoenOrdenjLabel;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable2;
    private javax.swing.JLabel precioTotaljLabel;
    private javax.swing.JPanel productosSeleccionadosjPanel;
    private javax.swing.JPanel seleccionProductosjPanel;
    private javax.swing.JTable selecciondeProdcutosjTable;
    private javax.swing.JLabel seleccioneProductosjLabel;
    private javax.swing.JLabel totalaPagarjLabel;
    // End of variables declaration//GEN-END:variables
}
