/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package situacion2.ui;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import situacion2.persistencia.Arrays;
import situacion2.Empresa;
import situacion2.IProducto;
import situacion2.Producto;
import situacion2.Proveedor;

/**
 *
 * @author Juan
 */
public class UIVentanaPrincipal extends javax.swing.JFrame {

    /**
     * Creates new form VetanaPrincipal
     */
    private final DefaultTableModel modelo= new DefaultTableModel(
    new Object [][] {

    },
    new String [] {
        "Codigo", "Tipo", "Nombre", "Cantidad", "Precio x U", "Funcionalidad", "Consumo Electrico"
    }
    ) {
       
        Class[] types = new Class [] {
            java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
        };
        boolean[] canEdit = new boolean [] {
            false, false, false, false, false, false, false
        };

        @Override
        public Class getColumnClass(int columnIndex) {
            return types [columnIndex];
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit [columnIndex];
        }
    };
    
    private final DefaultTableModel modelo1= new DefaultTableModel(
    new Object [][] {

    },
    new String [] {
        "CUIT", "Nombre", "Razon Social"
    }
    ) {
       
        Class[] types = new Class [] {
            java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
        };
        boolean[] canEdit = new boolean [] {
            false, false, false, false, false, false, false
        };

        @Override
        public Class getColumnClass(int columnIndex) {
            return types [columnIndex];
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit [columnIndex];
        }
    };
    
    private final DefaultTableModel modelo2= new DefaultTableModel(
    new Object [][] {

    },
    new String [] {
        "Codigo", "Tipo", "Nombre", "Cantidad", "Precio x U", "Nombre Proveedor","CUIT","Razon Social"
    }
    ) {
       
        Class[] types = new Class [] {
            java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class,java.lang.String.class,java.lang.String.class
        };
        boolean[] canEdit = new boolean [] {
            false, false, false, false, false, false,false,false
        };

        @Override
        public Class getColumnClass(int columnIndex) {
            return types [columnIndex];
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit [columnIndex];
        }
    };
    
    
    
    private Empresa empresa;
    private UICrearProducto v3;
    private UICrearOrdendeVenta v2;
    private UICrearProveedor v4;
    private UIVerordendeVenta v5;
    
    public UIVentanaPrincipal(Arrays persistencia) {
        String nombre;
        nombre=ingresoNombre();
        empresa = new Empresa(nombre, persistencia);
        this.setTitle(nombre);
        initComponents();
        productosjTable.getTableHeader().setReorderingAllowed(false);
        proveedoresjTable.getTableHeader().setReorderingAllowed(false);
        verMateriaPrimajTable.getTableHeader().setReorderingAllowed(false);
        ocultarComponentes();
    }
    
    private String ingresoNombre(){
        String nombre;
        do{
            nombre=JOptionPane.showInputDialog(this,"Ingrese nombre de la Empresa","Nombre empresa",1);
            if(nombre==null){
                System.exit(0);
            }
            if(("".equals(nombre))){
                JOptionPane.showMessageDialog(rootPane, "Ingresar nombre", "Alerta",0);
            }
        }while(nombre.equals(""));
        return nombre; 
    }

    private void ocultarComponentes(){
        ordenarjMenu.setVisible(false);
        CardLayoutJPanel.setVisible(false);
        panelProductosjPanel.setVisible(false);
        verProveedoresjPanel.setVisible(false);
        verMateriaPrimajPanel.setVisible(false);
    }
    
    private void llenarTablaProductos(ArrayList<IProducto> aux){
        for(int i = modelo.getRowCount() - 1; i >= 0; i--){
            modelo.removeRow(i);
        }
        for(IProducto x: aux){
                if(x.getCantidad()==0){
                    if(x.getTipo()=="Herramienta Electrica" || x.getTipo()=="Herramienta Manual" || x.getTipo()=="Material Fabricado"){
                    Object[] fila={x.getCodigo(),x.getTipo(),x.getNombre(),"SIN STOCK",x.getPrecio().toString(),x.getFuncionalidad(),x.getConsumo().toString()};
                    modelo.addRow(fila);
                }
                }else if(x.getTipo()=="Herramienta Electrica" || x.getTipo()=="Herramienta Manual" || x.getTipo()=="Material Fabricado"){
                    Object[] fila={x.getCodigo(),x.getTipo(),x.getNombre(),x.getCantidad(),x.getPrecio().toString(),x.getFuncionalidad(),x.getConsumo().toString()};
                    modelo.addRow(fila);
                }
                
        }
    
    }
    
    private void llenarTablaProductosOrdenados(ArrayList<Producto> aux){
        for(int i = modelo.getRowCount() - 1; i >= 0; i--){
            modelo.removeRow(i);
        }
        for(Producto x: aux){
                if(x.getCantidad()==0){
                    if(x.getTipo()=="Herramienta Electrica" || x.getTipo()=="Herramienta Manual" || x.getTipo()=="Material Fabricado"){
                    Object[] fila={x.getCodigo(),x.getTipo(),x.getNombre(),"SIN STOCK",x.getPrecio().toString(),x.getFuncionalidad(),x.getConsumo().toString()};
                    modelo.addRow(fila);
                }
                }else if(x.getTipo()=="Herramienta Electrica" || x.getTipo()=="Herramienta Manual" || x.getTipo()=="Material Fabricado"){
                    Object[] fila={x.getCodigo(),x.getTipo(),x.getNombre(),x.getCantidad(),x.getPrecio().toString(),x.getFuncionalidad(),x.getConsumo().toString()};
                    modelo.addRow(fila);
                }
                
        }
    
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        CardLayoutJPanel = new javax.swing.JPanel();
        verMateriaPrimajPanel = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        verMateriaPrimajTable = new javax.swing.JTable();
        verProveedoresjPanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        proveedoresjTable = new javax.swing.JTable();
        panelProductosjPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        productosjTable = new javax.swing.JTable();
        AccionesjMenuBar = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        verOrdendesdeVentajMenuItem = new javax.swing.JMenuItem();
        generarOrdendeVetnajMenuItem = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        verProductosStockjMenuItem = new javax.swing.JMenuItem();
        verMateriaPrimajMenuItem = new javax.swing.JMenuItem();
        agregarProductoOMPjMenuItem = new javax.swing.JMenuItem();
        ordenarjMenu = new javax.swing.JMenu();
        ordenarNombrejMenuItem = new javax.swing.JMenuItem();
        ordenarPreciojMenuItem = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        verProveedoresjMenuItem = new javax.swing.JMenuItem();
        agregarProveedoresjMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        CardLayoutJPanel.setLayout(new java.awt.CardLayout());

        verMateriaPrimajTable.setModel(modelo2
        );
        jScrollPane3.setViewportView(verMateriaPrimajTable);

        javax.swing.GroupLayout verMateriaPrimajPanelLayout = new javax.swing.GroupLayout(verMateriaPrimajPanel);
        verMateriaPrimajPanel.setLayout(verMateriaPrimajPanelLayout);
        verMateriaPrimajPanelLayout.setHorizontalGroup(
            verMateriaPrimajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 772, Short.MAX_VALUE)
        );
        verMateriaPrimajPanelLayout.setVerticalGroup(
            verMateriaPrimajPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
        );

        CardLayoutJPanel.add(verMateriaPrimajPanel, "card2");

        proveedoresjTable.setModel(modelo1);
        jScrollPane2.setViewportView(proveedoresjTable);

        javax.swing.GroupLayout verProveedoresjPanelLayout = new javax.swing.GroupLayout(verProveedoresjPanel);
        verProveedoresjPanel.setLayout(verProveedoresjPanelLayout);
        verProveedoresjPanelLayout.setHorizontalGroup(
            verProveedoresjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 772, Short.MAX_VALUE)
        );
        verProveedoresjPanelLayout.setVerticalGroup(
            verProveedoresjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
        );

        CardLayoutJPanel.add(verProveedoresjPanel, "card2");

        productosjTable.setModel(modelo
        );
        jScrollPane1.setViewportView(productosjTable);

        javax.swing.GroupLayout panelProductosjPanelLayout = new javax.swing.GroupLayout(panelProductosjPanel);
        panelProductosjPanel.setLayout(panelProductosjPanelLayout);
        panelProductosjPanelLayout.setHorizontalGroup(
            panelProductosjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 772, Short.MAX_VALUE)
        );
        panelProductosjPanelLayout.setVerticalGroup(
            panelProductosjPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
        );

        CardLayoutJPanel.add(panelProductosjPanel, "card3");
        panelProductosjPanel.setVisible(false);

        jMenu1.setText("Ordenes de Venta");

        verOrdendesdeVentajMenuItem.setText("Ver Ordenes de Venta");
        verOrdendesdeVentajMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                verOrdendesdeVentajMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(verOrdendesdeVentajMenuItem);

        generarOrdendeVetnajMenuItem.setText("Generar Orden de Venta");
        generarOrdendeVetnajMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generarOrdendeVetnajMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(generarOrdendeVetnajMenuItem);

        AccionesjMenuBar.add(jMenu1);

        jMenu2.setText("Stock");
        jMenu2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu2ActionPerformed(evt);
            }
        });

        verProductosStockjMenuItem.setText("Ver Productos en Stock");
        verProductosStockjMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                verProductosStockjMenuItemActionPerformed(evt);
            }
        });
        jMenu2.add(verProductosStockjMenuItem);

        verMateriaPrimajMenuItem.setText("Ver Materia Prima en Stock");
        verMateriaPrimajMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                verMateriaPrimajMenuItemActionPerformed(evt);
            }
        });
        jMenu2.add(verMateriaPrimajMenuItem);

        agregarProductoOMPjMenuItem.setText("Agregar Producto/Materia Prima");
        agregarProductoOMPjMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregarProductoOMPjMenuItemActionPerformed(evt);
            }
        });
        jMenu2.add(agregarProductoOMPjMenuItem);

        AccionesjMenuBar.add(jMenu2);

        ordenarjMenu.setText("Ordenar Stock por");

        ordenarNombrejMenuItem.setText("Nombre");
        ordenarNombrejMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ordenarNombrejMenuItemActionPerformed(evt);
            }
        });
        ordenarjMenu.add(ordenarNombrejMenuItem);

        ordenarPreciojMenuItem.setText("Precio");
        ordenarPreciojMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ordenarPreciojMenuItemActionPerformed(evt);
            }
        });
        ordenarjMenu.add(ordenarPreciojMenuItem);

        AccionesjMenuBar.add(ordenarjMenu);

        jMenu3.setText("Proveedores");
        jMenu3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu3ActionPerformed(evt);
            }
        });

        verProveedoresjMenuItem.setText("Ver proveedores");
        verProveedoresjMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                verProveedoresjMenuItemActionPerformed(evt);
            }
        });
        jMenu3.add(verProveedoresjMenuItem);

        agregarProveedoresjMenuItem.setText("Agregar Proveedores");
        agregarProveedoresjMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregarProveedoresjMenuItemActionPerformed(evt);
            }
        });
        jMenu3.add(agregarProveedoresjMenuItem);

        AccionesjMenuBar.add(jMenu3);

        setJMenuBar(AccionesjMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(CardLayoutJPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(CardLayoutJPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void agregarProductoOMPjMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregarProductoOMPjMenuItemActionPerformed
        v3 = new UICrearProducto(this, true, empresa);
        v3.setVisible(true);
    }//GEN-LAST:event_agregarProductoOMPjMenuItemActionPerformed

    private void verProductosStockjMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_verProductosStockjMenuItemActionPerformed
        CardLayoutJPanel.setVisible(true);
        verProveedoresjPanel.setVisible(false);
        panelProductosjPanel.setVisible(true);
        verMateriaPrimajPanel.setVisible(false);
        productosjTable.setVisible(true);
        ordenarjMenu.setVisible(true);
        productosjTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        ArrayList<IProducto> aux= new ArrayList<IProducto>();
        aux=(ArrayList<IProducto>)empresa.getProductos();
        llenarTablaProductos(aux);
    }//GEN-LAST:event_verProductosStockjMenuItemActionPerformed

    private void jMenu2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu2ActionPerformed
    }//GEN-LAST:event_jMenu2ActionPerformed

    private void verMateriaPrimajMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_verMateriaPrimajMenuItemActionPerformed
        CardLayoutJPanel.setVisible(true);
        ordenarjMenu.setVisible(false);
        verProveedoresjPanel.setVisible(false);
        panelProductosjPanel.setVisible(false);
        verMateriaPrimajPanel.setVisible(true);
        verMateriaPrimajTable.setVisible(true);
        verMateriaPrimajTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        ArrayList<IProducto> aux= new ArrayList<IProducto>();
        aux=(ArrayList<IProducto>)empresa.getProductos();
        for(int i = modelo2.getRowCount() - 1; i >= 0; i--){
            modelo2.removeRow(i);
        }
        for(IProducto x: aux){
                if("Materia Prima".equals(x.getTipo())){
                    Object[] fila={x.getCodigo(),x.getTipo(),x.getNombre(),x.getCantidad(),x.getPrecio().toString(),x.getProveedor().getNombre(),x.getProveedor().getCuit().toString(),x.getProveedor().getRazonSocal()};
                    modelo2.addRow(fila);
                }
                
        }
    }//GEN-LAST:event_verMateriaPrimajMenuItemActionPerformed

    private void generarOrdendeVetnajMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generarOrdendeVetnajMenuItemActionPerformed
        v2=new UICrearOrdendeVenta(this, true,empresa);
        v2.setVisible(true);
    }//GEN-LAST:event_generarOrdendeVetnajMenuItemActionPerformed

    private void agregarProveedoresjMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregarProveedoresjMenuItemActionPerformed
        v4=new UICrearProveedor(this, true, empresa);
        v4.setVisible(true);
    }//GEN-LAST:event_agregarProveedoresjMenuItemActionPerformed

    private void jMenu3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu3ActionPerformed
    }//GEN-LAST:event_jMenu3ActionPerformed

    private void verProveedoresjMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_verProveedoresjMenuItemActionPerformed
        CardLayoutJPanel.setVisible(true);
        ordenarjMenu.setVisible(false);
        verProveedoresjPanel.setVisible(true);
        panelProductosjPanel.setVisible(false);
        verMateriaPrimajPanel.setVisible(false);
        proveedoresjTable.setVisible(true);
        proveedoresjTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        ArrayList<Proveedor> aux= new ArrayList<Proveedor>();
        aux=(ArrayList<Proveedor>)empresa.getProveedores();
        for(int i = modelo1.getRowCount() - 1; i >= 0; i--){
            modelo1.removeRow(i);
        }
        for(Proveedor x: aux){
                Object[] fila={x.getCuit(),x.getNombre(),x.getRazonSocal()   };
                modelo1.addRow(fila);
        }
    }//GEN-LAST:event_verProveedoresjMenuItemActionPerformed

    private void verOrdendesdeVentajMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_verOrdendesdeVentajMenuItemActionPerformed
        v5=new UIVerordendeVenta(this, true, empresa);
        v5.setVisible(true);
    }//GEN-LAST:event_verOrdendesdeVentajMenuItemActionPerformed

    private void ordenarNombrejMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ordenarNombrejMenuItemActionPerformed
        ArrayList<Producto> aux;
        for(int i = modelo.getRowCount() - 1; i >= 0; i--){
            modelo.removeRow(i);
        }
        aux=empresa.ordenarProductosxNombre();
        llenarTablaProductosOrdenados(aux);
    }//GEN-LAST:event_ordenarNombrejMenuItemActionPerformed

    private void ordenarPreciojMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ordenarPreciojMenuItemActionPerformed
        ArrayList<Producto> aux;
        for(int i = modelo.getRowCount() - 1; i >= 0; i--){
            modelo.removeRow(i);
        }
        aux=empresa.ordenarProductosxPrecio();
        llenarTablaProductosOrdenados(aux);
    }//GEN-LAST:event_ordenarPreciojMenuItemActionPerformed

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar AccionesjMenuBar;
    private javax.swing.JPanel CardLayoutJPanel;
    private javax.swing.JMenuItem agregarProductoOMPjMenuItem;
    private javax.swing.JMenuItem agregarProveedoresjMenuItem;
    private javax.swing.JMenuItem generarOrdendeVetnajMenuItem;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JMenuItem ordenarNombrejMenuItem;
    private javax.swing.JMenuItem ordenarPreciojMenuItem;
    private javax.swing.JMenu ordenarjMenu;
    private javax.swing.JPanel panelProductosjPanel;
    private javax.swing.JTable productosjTable;
    private javax.swing.JTable proveedoresjTable;
    private javax.swing.JMenuItem verMateriaPrimajMenuItem;
    private javax.swing.JPanel verMateriaPrimajPanel;
    private javax.swing.JTable verMateriaPrimajTable;
    private javax.swing.JMenuItem verOrdendesdeVentajMenuItem;
    private javax.swing.JMenuItem verProductosStockjMenuItem;
    private javax.swing.JMenuItem verProveedoresjMenuItem;
    private javax.swing.JPanel verProveedoresjPanel;
    // End of variables declaration//GEN-END:variables
}
