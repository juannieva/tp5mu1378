/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package situacion2.ui;

import java.math.BigDecimal;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import situacion2.Empresa;
import situacion2.IProducto;
import situacion2.OrdenDeVenta;

/**
 *
 * @author Juan
 */
public class UIVerordendeVenta extends javax.swing.JDialog {

    DefaultTableModel modelo=new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Tipo", "Nombre", "Precio x U", "Cantidad", "Precio Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        };
    private Empresa empresa;
    public UIVerordendeVenta(java.awt.Frame parent, boolean modal,Empresa empresa) {
        super(parent, modal);
        this.empresa=empresa;
        initComponents();
        llenarComboBox();
        setTitle("Ver Ordenes de Venta");
    }
    
    private void llenarComboBox(){
        ArrayList<OrdenDeVenta> aux;
        aux=empresa.getOrdenesVenta();
        aux.forEach(x -> {
            numerodeOrdendeVentajComboBox.addItem(x.getCodigo().toString());
        });
    }
    private void llenarTabla(Integer codigo){
        OrdenDeVenta aux;
        aux=empresa.getOrdenVenta(codigo); 
        precioTotal2jLabel.setText(aux.getPrecioTotal().toString());
        ArrayList<IProducto> aux1=aux.getProductos();
        for(int i = modelo.getRowCount() - 1; i >= 0; i--){
            modelo.removeRow(i);
        }
        aux1.stream().map(x -> {
            BigDecimal auxiliar=x.getPrecio().multiply(new BigDecimal(x.getCantidad().toString()));
            Object[] fila={x.getCodigo().toString(),x.getTipo(),x.getNombre(),x.getPrecio().toString(),x.getCantidad(), auxiliar.toString()  };
            return fila;
        }).forEachOrdered(fila -> {
            modelo.addRow(fila);
        });
    
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        numerodeOrdendeVentajComboBox = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        precioTotaljLabel = new javax.swing.JLabel();
        precioTotal2jLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        numerodeOrdendeVentajComboBox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                numerodeOrdendeVentajComboBoxItemStateChanged(evt);
            }
        });
        numerodeOrdendeVentajComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numerodeOrdendeVentajComboBoxActionPerformed(evt);
            }
        });

        jLabel1.setText("Orden de Venta N�");

        jTable1.setModel(modelo
        );
        jTable1.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(jTable1);

        precioTotaljLabel.setText("Precio Total:");

        precioTotal2jLabel.setText("jLabel3");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(numerodeOrdendeVentajComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 573, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(precioTotaljLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(precioTotal2jLabel)
                .addGap(23, 23, 23))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(numerodeOrdendeVentajComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 297, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(precioTotaljLabel)
                    .addComponent(precioTotal2jLabel))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void numerodeOrdendeVentajComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numerodeOrdendeVentajComboBoxActionPerformed
 
    }//GEN-LAST:event_numerodeOrdendeVentajComboBoxActionPerformed

    private void numerodeOrdendeVentajComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_numerodeOrdendeVentajComboBoxItemStateChanged
        llenarTabla(Integer.parseInt(numerodeOrdendeVentajComboBox.getSelectedItem().toString()));
    }//GEN-LAST:event_numerodeOrdendeVentajComboBoxItemStateChanged

   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JComboBox<String> numerodeOrdendeVentajComboBox;
    private javax.swing.JLabel precioTotal2jLabel;
    private javax.swing.JLabel precioTotaljLabel;
    // End of variables declaration//GEN-END:variables
}
