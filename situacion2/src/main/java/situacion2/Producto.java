/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package situacion2;


import java.math.BigDecimal;

/**
 *
 * @author Juan
 */
public abstract class Producto implements  IProducto,Comparable<Producto>{
        private Integer codigo;
        private String nombre;
        private BigDecimal precio;
        private Integer cantidad;

        public Producto(Integer codigo, String nombre,Integer cantidad, BigDecimal precio) {
            this.codigo = codigo;
            this.nombre = nombre;
            this.cantidad = cantidad;
            this.precio = precio;
        }

        @Override
        public Integer getCodigo() {
            return codigo;
        }

        public void setCodigo(Integer codigo) {
            this.codigo = codigo;
        }

        @Override
        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }
        @Override
        public BigDecimal getPrecio() {
            return precio;
        }

        public void setPrecio(BigDecimal precio) {
            this.precio = precio;
        }
        @Override
        public abstract void mostrarProducto();

 
        @Override
        public Integer getCantidad() {
            return cantidad;
        }


        @Override
        public void setCantidad(Integer canti) {
            
            this.cantidad = canti;
        }
        
        @Override
        public int compareTo(Producto c){
        int resultado=0;
        resultado=this.precio.compareTo(c.precio);
        return resultado;
        

    }


}

