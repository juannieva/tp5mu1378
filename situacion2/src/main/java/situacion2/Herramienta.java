package situacion2;


import java.math.BigDecimal;

public abstract class Herramienta extends Producto {
    private String funcionalidad;

    public Herramienta(Integer codigo, String nombre,Integer cantidad, BigDecimal precio, String funcionalidad) {
        super(codigo, nombre,cantidad, precio);
        this.funcionalidad = funcionalidad;
    }
    
    @Override
    public String getFuncionalidad() {
        return funcionalidad;
    }

    public void setFuncionalidad(String funcionalidad) {
        this.funcionalidad = funcionalidad;
    }
    
    @Override
    public void mostrarProducto(){
    
    };
    

  
    
}
