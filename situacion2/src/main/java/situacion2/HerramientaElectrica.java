package situacion2;

import java.math.BigDecimal;

public class HerramientaElectrica extends Herramienta {
    private BigDecimal consumoElectrico;

    public HerramientaElectrica(Integer codigo, String nombre,Integer cantidad, BigDecimal precio, String funcionalidad,
            BigDecimal consumoElectrico) {
        super(codigo, nombre,cantidad, precio, funcionalidad);
        this.consumoElectrico = consumoElectrico;
    }
 
    public void setConsumoElectrico(BigDecimal consumoElectrico) {
        this.consumoElectrico = consumoElectrico;
    }

    @Override
    public void mostrarProducto(){
        System.out.println("Codigo: "+getCodigo()+"\nNombre: "+getNombre()+"\nPrecio: "+getPrecio()+"\nFuncionalidad: "+getFuncionalidad()+"\nConsumo: "+getConsumo()+"\n-------------------------------------------------------");
    }

    @Override
    public BigDecimal getConsumo() {
        return consumoElectrico;
    }

    @Override
    public String getTipo() {
        return "Herramienta Electrica";
    }

    @Override
    public Proveedor getProveedor() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
