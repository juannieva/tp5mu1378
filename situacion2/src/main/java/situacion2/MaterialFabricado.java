package situacion2;


import java.math.BigDecimal;

public class MaterialFabricado extends Producto {

    public MaterialFabricado(Integer codigo, String nombre,Integer cantidad, BigDecimal precio) {
        super(codigo, nombre,cantidad, precio);
    }
    
    @Override
    public void mostrarProducto() {
        System.out.println("Codigo: " + getCodigo() + "\nNombre: " + getNombre() + "\nPrecio: " + getPrecio()
                + "\n-------------------------------------------------------");

    }

    @Override
    public BigDecimal getConsumo() {
        return new BigDecimal("0");
    }

    @Override
    public String getFuncionalidad() {
        return "--";
    }

    @Override
    public String getTipo() {
        return "Material Fabricado";
    }

    @Override
    public Proveedor getProveedor() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
