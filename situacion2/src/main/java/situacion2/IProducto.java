package situacion2;

import java.math.BigDecimal;


public interface IProducto{
    
    public Integer getCodigo();
    public BigDecimal getConsumo();
    public String getFuncionalidad();
    public Integer getCantidad();
    public String getTipo();
    public String getNombre();
    public BigDecimal getPrecio();
    public void mostrarProducto();
    public void setCantidad(Integer cantidad);
    public Proveedor getProveedor();
    
}
