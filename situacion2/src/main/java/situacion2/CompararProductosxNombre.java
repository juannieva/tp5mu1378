package situacion2;

import java.util.Comparator;

public class CompararProductosxNombre implements Comparator<Producto> {
    @Override
    public int compare(Producto c1, Producto c2) {
        
        return (c1.getNombre().compareToIgnoreCase(c2.getNombre()));
    }
}
