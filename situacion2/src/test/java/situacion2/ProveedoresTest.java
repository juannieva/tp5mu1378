package situacion2;

import org.junit.Test;

import situacion2.persistencia.*;

import static org.junit.Assert.*;

import java.math.BigDecimal;

public class ProveedoresTest {

    @Test
    public void crearProveedor(){      
        Proveedor proveedor= new Proveedor(new BigDecimal("2065473"),"Jose Luis", "Comerciante");
        assertEquals("Jose Luis", proveedor.getNombre());
    }

    
    
}
