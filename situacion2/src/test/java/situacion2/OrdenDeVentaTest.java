package situacion2;

import org.junit.Test;

import situacion2.persistencia.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import static org.junit.Assert.*;

public class OrdenDeVentaTest {

    @Test
    public void crearOrdendeVenta(){      
        ArrayList<IProducto> productos=new ArrayList<IProducto>();
        IProducto herramientaManual2= new HerramientaManual(Integer.parseInt("11"), "Tenaza",Integer.parseInt("3"), new BigDecimal (70), "Sacar Clavos");
        IProducto herramientaElectrica2= new HerramientaElectrica(Integer.parseInt("33"), "Atornillador",Integer.parseInt("4"), new BigDecimal(1500), "Ajustar Tornillos", new BigDecimal(33));
        productos.add(herramientaElectrica2);
        productos.add(herramientaManual2);
        OrdenDeVenta orden1=new OrdenDeVenta(productos);
        assertEquals(Integer.parseInt("2"), orden1.getNumProductos(),0);
    }

    
    
}
