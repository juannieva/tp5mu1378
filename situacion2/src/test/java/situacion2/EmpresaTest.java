package situacion2;

import org.junit.Test;

import situacion2.persistencia.*;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.junit.Before;

public class EmpresaTest {
    Empresa empresa;
    IProducto materiaPrima1;
    IProducto materialFabricado1;
    IProducto herramientaManual1;
    IProducto herramientaElectrica1;
    @Test
    public void crearEmpresa(){      
        Arrays persistencia= new Arrays();  
        Empresa empresa=new Empresa("Total Gas",persistencia);
        assertEquals("Total Gas", empresa.getNombre());
    }
    @Before
    public void iniciarEmpresa(){      
        Arrays persistencia= new Arrays();  
        empresa=new Empresa("Total Gas",persistencia);
    }

    @Test
    public void agregarProductos(){      
        Arrays persistencia= new Arrays();  
        Empresa empresa=new Empresa("Total Gas",persistencia);
        IProducto materiaPrima1= new MateriaPrima(Integer.parseInt("56"), "Arena", Integer.parseInt("2"), new BigDecimal(250), (Proveedor) persistencia.get(2).findByPK(Integer.parseInt("2065473")));
        IProducto materialFabricado1= new MaterialFabricado(Integer.parseInt("854"), "Cemento",Integer.parseInt("500") ,new BigDecimal(250));
        IProducto herramientaManual1= new HerramientaManual(Integer.parseInt("22"), "Tenaza",Integer.parseInt("340"), new BigDecimal (70), "Sacar Clavos");
        IProducto herramientaElectrica1= new HerramientaElectrica(Integer.parseInt("521"), "Atornillador",Integer.parseInt("149"), new BigDecimal(1500), "Ajustar Tornillos", new BigDecimal(33));
        empresa.agregarProducto(materiaPrima1);
        empresa.agregarProducto(materialFabricado1);
        empresa.agregarProducto(herramientaManual1);
        empresa.agregarProducto(herramientaElectrica1);
        assertEquals(Integer.parseInt("4"), empresa.getNumProductos(),0);
    }

    @Test
    public void agregarProveedores(){      
        Arrays persistencia= new Arrays();  
        Empresa empresa=new Empresa("Total Gas",persistencia);
        Proveedor proveedor= new Proveedor(new BigDecimal("2065473"),"Jose Luis", "Comerciante");
        Proveedor proveedor1= new Proveedor(new BigDecimal("10654732"),"Marcelo Diaz", "Comerciante");
        empresa.agregarProveedor(proveedor);
        empresa.agregarProveedor(proveedor1);
        assertEquals(Integer.parseInt("2"), empresa.getNumProveedores(),0);
    }

    @Test
    public void agregarOrdenDeVenta(){      
        Arrays persistencia= new Arrays();  
        Empresa empresa=new Empresa("Total Gas",persistencia);
        ArrayList<IProducto> productos=new ArrayList<IProducto>();
        IProducto herramientaManual2= new HerramientaManual(Integer.parseInt("11"), "Tenaza",Integer.parseInt("3"), new BigDecimal (70), "Sacar Clavos");
        IProducto herramientaElectrica2= new HerramientaElectrica(Integer.parseInt("33"), "Atornillador",Integer.parseInt("4"), new BigDecimal(1500), "Ajustar Tornillos", new BigDecimal(33));
        productos.add(herramientaElectrica2);
        productos.add(herramientaManual2);
        OrdenDeVenta orden1=new OrdenDeVenta(productos);
        empresa.agregarOrdenesDeVenta(orden1);
        assertEquals(Integer.parseInt("1"), empresa.getNumOrdendeVenta(),0);
    }
    
    
}
