package situacion2;

import org.junit.Test;

import situacion2.persistencia.*;

import static org.junit.Assert.*;

import java.math.BigDecimal;

public class IProductoTest {

    @Test
    public void crearHerramientaManual(){      
        IProducto herramientaManual1= new HerramientaManual(Integer.parseInt("22"), "Tenaza",Integer.parseInt("340"), new BigDecimal (70), "Sacar Clavos");
        assertEquals(Integer.parseInt("22"), herramientaManual1.getCodigo(),0);
    }

    @Test
    public void crearHerramientaElectrica(){      
        IProducto herramientaElectrica1= new HerramientaElectrica(Integer.parseInt("521"), "Atornillador",Integer.parseInt("149"), new BigDecimal(1500), "Ajustar Tornillos", new BigDecimal(33));
        assertEquals(Integer.parseInt("521"), herramientaElectrica1.getCodigo(),0);
    }

    @Test
    public void crearMaterialFabricado(){      
        IProducto materialFabricado1= new MaterialFabricado(Integer.parseInt("854"), "Cemento",Integer.parseInt("500") ,new BigDecimal(250));
        assertEquals(Integer.parseInt("854"), materialFabricado1.getCodigo(),0);
    }

    @Test
    public void crearMaterialPrima(){      
        IProducto materiaPrima1= new MateriaPrima(Integer.parseInt("56"), "Arena", Integer.parseInt("2"), new BigDecimal(250), new Proveedor(new BigDecimal("10654732"),"Marcelo Diaz", "Comerciante"));
        assertEquals(Integer.parseInt("56"), materiaPrima1.getCodigo(),0);
    }
    
}
