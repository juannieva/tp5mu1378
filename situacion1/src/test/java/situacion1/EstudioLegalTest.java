package situacion1;

import org.junit.Test;

import situacion1.persistencia.*;
import situacion1.situacion1Excepciones.*;

import static org.junit.Assert.*;

public class EstudioLegalTest {

    @Test
    public void crearEstudioLegalyAgregar1Cliente(){        
        ICuentas cuentaDAO = new ArrayCuentas();
        EstudioLegal estudiolegal=new EstudioLegal(cuentaDAO);
        estudiolegal.agregarClientes(new Cliente("jose", "Roberto",Integer.parseInt("234") , new DomicilioSimple("villacuba", Integer.parseInt("123")), "as@gasd.com", Integer.parseInt("99812312")));
        Integer aux=estudiolegal.getNumClientes();
        assertEquals(Integer.parseInt("1"),aux,0);
    }

    @Test
    public void Agregar2Cliente(){        
        ICuentas cuentaDAO = new ArrayCuentas();
        EstudioLegal estudiolegal=new EstudioLegal(cuentaDAO);
        estudiolegal.agregarClientes(new Cliente("jose", "Roberto",Integer.parseInt("234") , new DomicilioSimple("villacuba", Integer.parseInt("123")), "as@gasd.com", Integer.parseInt("99812312")));
        estudiolegal.agregarClientes(new Cliente("Roberto", "Jesus",Integer.parseInt("134") , new DomicilioSimple("villacuba", Integer.parseInt("123")), "a213s@gasd.com", Integer.parseInt("33412312")));
        Integer aux=estudiolegal.getNumClientes();
        assertEquals(Integer.parseInt("2"),aux,0);
    }

    @Test (expected = ClienteExistenteException.class)
    public void Agregar2ClienteIguales(){        
        ICuentas cuentaDAO = new ArrayCuentas();
        EstudioLegal estudiolegal=new EstudioLegal(cuentaDAO);
        estudiolegal.agregarClientes(new Cliente("jose", "Roberto",Integer.parseInt("234") , new DomicilioSimple("villacuba", Integer.parseInt("123")), "as@gasd.com", Integer.parseInt("99812312")));
        estudiolegal.agregarClientes(new Cliente("jose", "Roberto",Integer.parseInt("234") , new DomicilioSimple("villacuba", Integer.parseInt("123")), "as@gasd.com", Integer.parseInt("99812312")));
        Integer aux=estudiolegal.getNumClientes();
        assertEquals(Integer.parseInt("2"),aux,0);
    }
    
}
