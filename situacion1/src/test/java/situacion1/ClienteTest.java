package situacion1;

import org.junit.Test;
import static org.junit.Assert.*;

public class ClienteTest {

    @Test
    public void crearClienteconDomicilioSimple(){        
        Cliente cliente1=new Cliente("jose", "Roberto",Integer.parseInt("234") , new DomicilioSimple("villacuba", Integer.parseInt("123")), "as@gasd.com", Integer.parseInt("99812312"));
        assertEquals("jose",cliente1.getNombre());
    }

    @Test
    public void crearClienteconDomicilioEdificio(){        
        Cliente cliente1=new Cliente("jose", "Roberto",Integer.parseInt("234") , new DomicilioEdificio("avenida Ocampo",Integer.parseInt("123") , Integer.parseInt("2"), Integer.parseInt("23")), "as@gasd.com", Integer.parseInt("99812312"));
        assertEquals("jose",cliente1.getNombre());
    }

    @Test
    public void crearClienteconDomicilioBarrial(){        
        Cliente cliente1=new Cliente("jose", "Roberto",Integer.parseInt("234") ,new DomicilioBarrial("avenida Ocampo",Integer.parseInt("123") , "Manzana B"), "as@gasd.com", Integer.parseInt("99812312"));
        assertEquals("jose",cliente1.getNombre());
    }
    
}
