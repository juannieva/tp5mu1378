package situacion1;


import org.junit.Test;
import static org.junit.Assert.*;


public class DomicilioTest {

    @Test
    public void crearDomicilioSimple(){

        Domicilio domicilioSimple= new DomicilioSimple("villacuba", Integer.parseInt("123"));

        assertEquals(Integer.parseInt("123"), domicilioSimple.getNumero(),0);
    }

    @Test
    public void crearDomicilioEdificio(){
        
        DomicilioEdificio domicilioEdificio= new DomicilioEdificio("avenida Ocampo",Integer.parseInt("123") , Integer.parseInt("2"), Integer.parseInt("23"));
        Domicilio aux= domicilioEdificio;
        assertEquals(Integer.parseInt("2"), domicilioEdificio.getPiso(),0);
    }

    @Test
    public void crearDomicilioBarrial(){
        
        DomicilioBarrial domicilioBarrial= new DomicilioBarrial("avenida Ocampo",Integer.parseInt("123") , "Manzana B");
        Domicilio aux= domicilioBarrial;
        assertEquals("Manzana B", domicilioBarrial.getManzana());
    }
    
}
