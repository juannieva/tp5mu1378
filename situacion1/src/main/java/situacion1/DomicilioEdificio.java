package situacion1;

public class DomicilioEdificio extends Domicilio {
    private Integer piso;
    private Integer departamento;
    
    public DomicilioEdificio(String calleoNombreBarrio, Integer numero, Integer piso, Integer departamento) {
        super(calleoNombreBarrio, numero);
        this.piso = piso;
        this.departamento = departamento;
    }

    public String mostrarDomicilio(){
        return (super.getCalleoNombreBarrio()+",Numero:"+super.getNumero()+",Piso:"+piso+",Departamento:"+departamento);
    }
    public Integer getPiso() {
        return piso;
    }

    public void setPiso(Integer piso) {
        this.piso = piso;
    }

    public Integer getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Integer departamento) {
        this.departamento = departamento;
    }

    
    
}
