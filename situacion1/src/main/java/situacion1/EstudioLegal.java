package situacion1;

import java.util.ArrayList;
import java.util.Collections;
import situacion1.persistencia.ICuentas;
import situacion1.situacion1Excepciones.ClienteNoEncontradoExceptio;

public class EstudioLegal {
    
    private final ICuentas cuentaDAO;

    public EstudioLegal(ICuentas cuentaDao){
        this.cuentaDAO=cuentaDao;
    }
    public Integer numClientes(){
        return cuentaDAO.findAll().size();
    }
    public Cliente buscarClientexDni(Integer dni){
        Cliente clienteEncontrado=cuentaDAO.findByPK(dni);
        if (clienteEncontrado==null){
            throw new ClienteNoEncontradoExceptio("Cliente Inexistente: "+dni);
        }
        return clienteEncontrado;
    }
    
    public void agregarClientes(Cliente cliente){
        cuentaDAO.insert(cliente);
    }

    public ArrayList<Cliente> getClientes(){
        return (ArrayList) cuentaDAO.findAll();
    }

    public Integer getNumClientes(){
        Integer i=cuentaDAO.findAll().size();
        return i;
    }

    public ArrayList<Cliente> imprimirClientesxDni(){
        ArrayList<Cliente> clientes = (ArrayList<Cliente>) cuentaDAO.findAll();
        Collections.sort(clientes);
        return clientes;
        
    }

    public ArrayList<Cliente> imprimirClientesxNombre(){
        ArrayList<Cliente> clientes = (ArrayList<Cliente>) cuentaDAO.findAll();
        Collections.sort(clientes, new CompararClientexNombre());
        return clientes;        
    }
}
