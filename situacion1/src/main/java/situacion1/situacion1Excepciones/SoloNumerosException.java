/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package situacion1.situacion1Excepciones;

/**
 *
 * @author Juan
 */
public class SoloNumerosException extends RuntimeException{
    
    public SoloNumerosException(String mensaje){
        super("Solo Numeros permitido en campo "+mensaje);
    }
    
}
