/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package situacion1.situacion1Excepciones;

/**
 *
 * @author Juan
 */
public class SoloLetrasException extends RuntimeException{
    
    public SoloLetrasException(String mensaje){
        super("Solo letras permitido en campo "+mensaje);
    }
    
}
