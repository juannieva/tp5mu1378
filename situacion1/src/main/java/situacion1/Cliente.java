package situacion1;

import situacion1.situacion1Excepciones.SoloLetrasException;
import situacion1.situacion1Excepciones.SoloNumerosException;

public class Cliente implements Comparable<Cliente> {
    private String nombre;
    private String apellido;
    private Integer dni;
    private Domicilio domicilio;
    private String correoElectronico;
    private Integer telefono;

    public Cliente(String nombre, String apellido, Integer dni, Domicilio domicilio, String correoElectronico,
    Integer telefono) {
        if(!esSoloLetras(nombre)||!esSoloLetras(apellido)){
            if(!esSoloLetras(nombre)){
                throw new SoloLetrasException("nombre");
            }else{
                throw new SoloLetrasException("apellido");
            }
        }
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.domicilio = domicilio;
        this.correoElectronico = correoElectronico;
        this.telefono = telefono;
    }

 

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getDni() {
        return dni;
    }

    public void setDni(Integer dni) {
        this.dni = dni;
    }

    public Domicilio getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(Domicilio domicilio) {
        this.domicilio = domicilio;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public Integer getTelefono() {
        return telefono;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    
    @Override
    public int compareTo(Cliente c){
        int resultado=0;
        if (this.dni<c.dni) {  resultado = -1;    }
            else if (this.dni>c.dni) {   resultado = 1;   }
        else {   resultado = 0;   }
        return resultado;
        

    }
    
    static boolean esSoloLetras(String cadena)
    {
		for (int i = 0; i < cadena.length(); i++)
		{
			char caracter = cadena.toUpperCase().charAt(i);
			int valorASCII = (int)caracter;
			if (valorASCII != 165 && (valorASCII < 65 || valorASCII > 90))
				return false; 
		}
		return true;
    }
    static boolean esSoloNumeros(Integer caden)
    {           
                String cadena=caden.toString();
		for (int i = 0; i < cadena.length(); i++)
		{
			char caracter = cadena.charAt(i);
			int valorASCII = (int)caracter;
			if (valorASCII < 48 || valorASCII > 57)
				return false; 
		}
		return true;
    }
}
