package situacion1;

public class DomicilioSimple extends Domicilio{

    public DomicilioSimple(String calle, Integer numero){
        super(calle, numero);
    }

    public String mostrarDomicilio(){
        return (super.getCalleoNombreBarrio()+",Numero:"+super.getNumero());
    }
    
}
