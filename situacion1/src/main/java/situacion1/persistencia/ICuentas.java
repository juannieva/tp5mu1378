/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package situacion1.persistencia;

import java.util.Collection;
import situacion1.Cliente;

/**
 *
 * @author Juan
 */
public interface ICuentas {
    public void delete(String vnumero);
    public Cliente findByPK(Integer vnumero);
    public Collection findAll();
    public void insert(Cliente insertRecord);
    public void update(Cliente updateRecord);
}
