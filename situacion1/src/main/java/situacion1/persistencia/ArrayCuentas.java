/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package situacion1.persistencia;

import java.util.ArrayList;
import java.util.Collection;
import situacion1.Cliente;
import situacion1.situacion1Excepciones.ClienteExistenteException;
import situacion1.situacion1Excepciones.ClienteNoEncontradoExceptio;

/**
 *
 * @author Juan
 */
public class ArrayCuentas implements ICuentas {
    private ArrayList<Cliente> clientes = new ArrayList<Cliente>();
    
    @Override
    public void delete(String dni) {
       Cliente existe = findByPK(Integer.parseInt(dni));
        if (existe != null) {
            clientes.remove(existe);            
            return;
        }        
        throw new ClienteNoEncontradoExceptio("Dni inexistente: " + dni);
    }
    @Override
    public Cliente findByPK(Integer dni)  {
        Cliente resultado = null;
         for (Cliente aux : clientes) {
            if (aux.getDni().equals(dni)){
                resultado=aux;
            }
        }
        if(resultado==null){
        throw new ClienteNoEncontradoExceptio("Cliente DNI:"+dni+" no encontrado");
        }
        return resultado;
    }
    
    

    @Override
    public Collection findAll() {
        return clientes;
    }
    
    public ArrayList<Cliente> retornarClientes() {
        return clientes;
    }

    @Override
    public void insert(Cliente insertRecord) {
        Cliente existe=null;
        for (Cliente aux : clientes) {
            if (aux.getDni().equals(insertRecord.getDni())){
                existe=aux;
            }
        }
        if (existe != null) {
            throw new ClienteExistenteException("Cliente existente DNI:" + existe.getDni().toString());
        }
        clientes.add(insertRecord);
    }

    @Override
    public void update(Cliente updateRecord) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
