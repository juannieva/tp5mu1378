package situacion1;

public abstract class Domicilio {
    private String calleoNombreBarrio;
    private Integer numero;

    public Domicilio(String calleoNombreBarrio, Integer numero){
        this.calleoNombreBarrio=calleoNombreBarrio;
        this.numero= numero;
    }

    public abstract String mostrarDomicilio();

    public String getCalleoNombreBarrio() {
        return calleoNombreBarrio;
    }

    public void setCalleoNombreBarrio(String calleoNombreBarrio) {
        this.calleoNombreBarrio = calleoNombreBarrio;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }
    
    
}
